
#include "Zipf.hpp"

//-----------------------------------------------------------------------------
int main(int argn, char *argv[])
{
    using namespace zipf;

    Options options;

    try {
        processOptions(options, argn, argv);
        generate<std::mt19937_64>(options);
    } catch (const std::exception& e) {
        std::cerr << "[ERROR] " << e.what() << "\n";
        return -1;
    }
}
