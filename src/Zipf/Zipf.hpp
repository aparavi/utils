
#include <iostream>
#include <random>
#include <functional>
#include <filesystem>
#include <string>
#include <string_view>
#include <memory>
#include <vector>
#include <array>
#include <fstream>
#include <sstream>
#include <cmath>
#include <algorithm>
#include <optional>
#include <stdexcept>
#include <set>
#include <assert.h>

namespace zipf {

using namespace std::string_literals;
using namespace std::string_view_literals;

using Blob = std::vector<std::byte>;
using BlobUniPtr = std::unique_ptr<Blob>;

using SS = std::stringstream;

using StringViewVector = std::vector<std::string_view>;
using StringViewVectorUniPtr = std::unique_ptr<StringViewVector>;


// example database file: https://github.com/dwyl/english-words (words.txt)

// forwards
void throwStdException(const std::stringstream& ss) noexcept(false);

//-----------------------------------------------------------------------------
struct Options
{
    std::uint64_t seed_{};
    std::filesystem::path wordsFilePath_;
    std::filesystem::path saveSortedWordsFilePath_;
    std::filesystem::path saveZipfWordsFilePath_;
    std::vector<std::filesystem::path> outputFileFilePaths_;
    std::vector<std::filesystem::path> outputPaths_;
    std::vector<std::size_t> totalPaths_;
    std::vector<std::size_t> totalPathFiles_;
    bool wordsSorted_{};
    bool wordsZipf_{};
    std::size_t maxWordsPerFile_{0xFFFF};
    bool zipfDistribution_{ true };
    std::optional<std::size_t> quotaInBytes_{};
    bool validateOnly_{};
};

//-----------------------------------------------------------------------------
inline void processOptions(
    Options& outOptions,
    int argn,
    char* argv[]) noexcept(false)
{
    constexpr static std::string_view switchPrefix{ "--"sv };
    constexpr static std::string_view switchHelp{ "help"sv };
    constexpr static std::string_view switchSeed{ "seed"sv };
    constexpr static std::string_view switchWords{ "words"sv };
    constexpr static std::string_view switchWordsSorted{ "words-sorted"sv };
    constexpr static std::string_view switchWordsZipf{ "words-zipf"sv };
    constexpr static std::string_view switchSaveSorted{ "save-sorted"sv };
    constexpr static std::string_view switchSaveZipf{ "save-zipf"sv };
    constexpr static std::string_view switchOutputFile{ "out"sv };
    constexpr static std::string_view switchOutputPath{ "dir"sv };
    constexpr static std::string_view switchMaxWordPerFile{ "max-words"sv };
    constexpr static std::string_view switchUniform{ "uniform"sv };
    constexpr static std::string_view switchZipf{ "zipf"sv };
    constexpr static std::string_view switchQuota{ "quota"sv };
    constexpr static std::string_view switchTotalPaths{ "total-paths"sv };
    constexpr static std::string_view switchTotalPathFiles{ "total-path-files"sv };
    constexpr static std::string_view switchValidateOnly{ "validate-only"sv };

    struct SwitchInfo {
        std::string_view switch_;
        std::string_view arg_;
        std::string_view help1_;
        std::string_view help2_;
    };

    constexpr static std::array<SwitchInfo, 16> allSwitches = { {
        {switchHelp, ""sv, "display help information"sv },
        {switchSeed, "<seed-string>"sv, "sets a seed value for RNG"sv },
        {switchWords, "<file>"sv, "location of words text file"sv },
        {switchWordsSorted, ""sv, "indicates the words text file is a pre-sorted words file"sv },
        {switchWordsZipf, ""sv, "indicates the words text file is a pre-generated zipf distributed file"sv },
        {switchSaveSorted, "<file>"sv, "saves the words text file post-sorting"sv },
        {switchSaveZipf, "<file>"sv, "saves the chosen zipf words as a text file"sv, "(only allowed if using zipf distribution)" },
        {switchOutputFile, "<file(s)...>"sv, "generates a Zipf file"sv },
        {switchOutputPath, "<path(s)...>"sv, "generates subdirectories of zipf files inside specified path"sv },
        {switchMaxWordPerFile, "<value>"sv, "max zipf words per file (default is 65535)"sv },
        {switchUniform, ""sv, "generate words with uniform distribution from words file"sv, "(using fully randomized words instead of zipf distribution)"sv },
        {switchZipf, ""sv, "generate words with Zipf distribution from words file (default)"sv },
        {switchQuota, "<megabytes>"sv, "sets a maximum output quota size (in MB)"sv },
        {switchTotalPaths, "<total...>"sv, "specified the number of sub-directories to create at each path depth"sv },
        {switchTotalPathFiles, "<total...>"sv, "specified the number of files to generate at each path depth"sv },
        {switchValidateOnly, ""sv, "validates previously generated output was created properly"sv, "(but will not generate any output)"sv }
    } };

    std::string lastSwitch;
    std::string repeatLastSwitch;

    auto displayHelp = [&]() noexcept {
        std::cout << "(c)2020 Aparavi. All rights reserved.\n";
        std::cout << "\n";
        std::cout << ""
            "Using zipf's law (https://en.wikipedia.org/wiki/Zipf%27s_law), this tool\n"
            "generates randomized word files and/or randomized folders filled with\n"
            "randomized word files.\n";
        std::cout << "\n";
        std::cout << "Usage:\n" << argv[0] << " " << switchPrefix << switchSeed << " <seed> --" << switchWords << " <words-fils> " << switchPrefix << switchOutputFile << " <files...>" << "\n";
        std::cout << "\n";
        for (auto& switchInfo : allSwitches) {
            std::cout << switchPrefix << std::setw(16) << std::left << switchInfo.switch_ << " " << std::setw(15) << std::left << switchInfo.arg_ << " " << std::setw(0) << switchInfo.help1_ << "\n";
            if (!switchInfo.help2_.empty()) {
                std::cout << std::setw(switchPrefix.length()) << " " << std::setw(33) << " " << std::setw(0) << switchInfo.help2_ << "\n";
            }
        }
        std::cout << "\n";
    };

    if (argn == 1)
        displayHelp();

    for (decltype(argn) loop{ 1 }; loop < argn; ++loop) {
        std::string arg{ argv[loop] };

        if (arg.substr(0, 2) == switchPrefix) {
            repeatLastSwitch.clear();
            auto switchValue = arg.substr(switchPrefix.length());
            lastSwitch = switchValue;

            if (switchWordsSorted == lastSwitch) {
                outOptions.wordsSorted_ = true;
                lastSwitch.clear();
            }
            else if (switchWordsZipf == lastSwitch) {
                outOptions.wordsZipf_ = true;
                lastSwitch.clear();
            }
            else if (switchHelp == lastSwitch) {
                lastSwitch.clear();
                displayHelp();
            }
            else if (switchUniform == lastSwitch) {
                outOptions.zipfDistribution_ = false;
                lastSwitch.clear();
            }
            else if (switchZipf == lastSwitch) {
                outOptions.zipfDistribution_ = true;
                lastSwitch.clear();
            }
            else if (switchValidateOnly == lastSwitch) {
                outOptions.validateOnly_ = true;
                lastSwitch.clear();
            }
            continue;
        }
        else {
            if (!repeatLastSwitch.empty()) {
                lastSwitch = repeatLastSwitch;
            }
        }

        if (lastSwitch.empty()) {
            lastSwitch = arg;
            arg.clear();
        }

        if (switchSeed == lastSwitch) {
            // https://stackoverflow.com/questions/7666509/hash-function-for-string
            auto hashCalc = [](const char* str) -> decltype(outOptions.seed_) {
                decltype(outOptions.seed_) hash = 5381;
                decltype(outOptions.seed_) c{};

                for (c = static_cast<decltype(c)>(*str); *str; c = static_cast<decltype(c)>(*str), ++str)
                    hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

                return hash;
            };

            outOptions.seed_ = hashCalc(arg.c_str());
        }
        else if (switchWords == lastSwitch) {
            outOptions.wordsFilePath_ = arg;
        }
        else if (switchSaveSorted == lastSwitch) {
            outOptions.saveSortedWordsFilePath_ = arg;
        }
        else if (switchSaveZipf == lastSwitch) {
            outOptions.saveZipfWordsFilePath_ = arg;
        }
        else if (switchOutputFile == lastSwitch) {
            outOptions.outputFileFilePaths_.emplace_back(arg);
            repeatLastSwitch = lastSwitch;
        }
        else if (switchMaxWordPerFile == lastSwitch) {
            outOptions.maxWordsPerFile_ = static_cast<decltype(outOptions.maxWordsPerFile_)>(std::stoull(arg));
        }
        else if (switchOutputPath == lastSwitch) {
            outOptions.outputPaths_.emplace_back(arg);
            repeatLastSwitch = lastSwitch;
        }
        else if (switchQuota == lastSwitch) {
            outOptions.quotaInBytes_ = static_cast<decltype(outOptions.quotaInBytes_)::value_type>(std::stoull(arg)) * static_cast<std::size_t>(1024 * 1024);
        }
        else if (switchTotalPaths == lastSwitch) {
            outOptions.totalPaths_.emplace_back(static_cast<decltype(outOptions.totalPaths_)::value_type>(std::stoull(arg)));
            repeatLastSwitch = lastSwitch;
        }
        else if (switchTotalPathFiles == lastSwitch) {
            outOptions.totalPathFiles_.emplace_back(static_cast<decltype(outOptions.totalPathFiles_)::value_type>(std::stoull(arg)));
            repeatLastSwitch = lastSwitch;
        }
        else {
            throwStdException(SS{} << "switch is not understood: " << lastSwitch);
        }

        lastSwitch.clear();
    }

    if (!lastSwitch.empty())
        throwStdException(SS{} << "switch expecting a value: " << lastSwitch);

    if ((outOptions.wordsZipf_) &&
        (outOptions.wordsSorted_))
        throwStdException(SS{} << switchPrefix << switchZipf << " must not be used with " << switchPrefix << switchWordsSorted);

    if ((!outOptions.saveZipfWordsFilePath_.empty()) &&
        (!outOptions.zipfDistribution_))
        throwStdException(SS{} << switchPrefix << switchZipf << " must be used with " << switchPrefix << switchSaveZipf);

    if ((!outOptions.saveZipfWordsFilePath_.empty()) &&
        (outOptions.wordsZipf_))
        throwStdException(SS{} << switchPrefix << switchZipf << " must not be used with " << switchPrefix << switchSaveZipf);

    if ((outOptions.totalPaths_.size() > 0) ||
        (outOptions.totalPathFiles_.size() > 0)) {
        if (outOptions.totalPaths_.size() + 1 != outOptions.totalPathFiles_.size())
            throwStdException(SS{} << "number of elements specified for " << switchPrefix << switchTotalPathFiles << " must be one element larger than " << switchPrefix << switchTotalPaths);
    }

    if (!outOptions.outputPaths_.empty()) {
        if (outOptions.totalPaths_.size() < 1) {
            throwStdException(SS{} << "usage of " << switchPrefix << switchOutputPath << " requires " << switchPrefix << switchTotalPaths << " and " << switchPrefix << switchTotalPathFiles);
        }
    }
}

//https://stackoverflow.com/questions/9983239/how-to-generate-zipf-distributed-numbers-efficiently
/** Zipf-like random distribution.
 *
 * "Rejection-inversion to generate variates from monotone discrete
 * distributions", Wolfgang Hörmann and Gerhard Derflinger
 * ACM TOMACS 6.3 (1996): 169-184
 */
template<class IntType = std::size_t, class RealType = double>
class zipf_distribution
{
public:
    using input_type = RealType;
    using result_type = IntType;

    static_assert(std::numeric_limits<IntType>::is_integer);
    static_assert(!std::numeric_limits<RealType>::is_integer);

    zipf_distribution(
        const IntType n = std::numeric_limits<IntType>::max(),
        const RealType q = 1.0) noexcept :
        n_(n),
        q_(q),
        H_x1_(H(1.5) - 1.0),
        H_n_(H(n + 0.5)),
        H_diff(H_n_ - H_x1_)
    {}

    template <typename TGenerator>
    IntType operator()(TGenerator& rng) noexcept
    {
        while (true) {
            const RealType u = dist(rng);
            const RealType x = H_inv(u);
            const IntType  k = clamp<IntType>(static_cast<IntType>(std::round(x)), 1, n_);
            if (u >= H(k + 0.5) - h(static_cast<RealType>(k))) {
                return k;
            }
        }
    }

private:
    /** Clamp x to [min, max]. */
    template<typename T>
    static constexpr T clamp(const T x, const T min, const T max) noexcept
    {
        return std::max(min, std::min(max, x));
    }

    /** exp(x) - 1 / x */
    static double expxm1bx(const double x) noexcept
    {
        return (std::abs(x) > epsilon)
            ? std::expm1(x) / x
            : (1.0 + x / 2.0 * (1.0 + x / 3.0 * (1.0 + x / 4.0)));
    }

    /** H(x) = log(x) if q == 1, (x^(1-q) - 1)/(1 - q) otherwise.
     * H(x) is an integral of h(x).
     *
     * Note the numerator is one less than in the paper order to work with all
     * positive q.
     */
    const RealType H(const RealType x) noexcept
    {
        const RealType log_x = std::log(x);
        return expxm1bx((1.0 - q_) * log_x) * log_x;
    }

    /** log(1 + x) / x */
    static RealType log1pxbx(const RealType x) noexcept
    {
        return (std::abs(x) > epsilon)
            ? std::log1p(x) / x
            : 1.0 - x * ((1 / 2.0) - x * ((1 / 3.0) - x * (1 / 4.0)));
    }

    /** The inverse function of H(x) */
    const RealType H_inv(const RealType x) noexcept
    {
        const RealType t = std::max(-1.0, x * (1.0 - q_));
        return std::exp(log1pxbx(t) * x);
    }

    /** That hat function h(x) = 1 / (x ^ q) */
    const RealType h(const RealType x) noexcept
    {
        return std::exp(-q_ * std::log(x));
    }

    static constexpr RealType maxIntTypeAsReal{ static_cast<RealType>(std::numeric_limits<IntType>::max()) };
    static constexpr RealType epsilon{ 1e-8 };

    template <typename TGenerator>
    RealType dist(TGenerator &rng) noexcept
    {
        const IntType d = dist_(rng) % std::numeric_limits<IntType>::max();
        auto result = ((static_cast<RealType>(d) / maxIntTypeAsReal) * H_diff) + H_x1_;
#if 0
        std::cout << __LINE__ << " RESULT: " << d << " " << result << " " << H_n_ << " " << H_x1_ << "\n";
#endif //0
        return result;
    }

    IntType                                  n_;     ///< Number of elements
    RealType                                 q_;     ///< Exponent
    RealType                                 H_x1_;  ///< H(x_1)
    RealType                                 H_n_;   ///< H(n)
    RealType                                 H_diff; ///< H_n_ - H_x1_
    std::uniform_int_distribution<IntType>   dist_;  ///< [H(x_1), H(n)]
};

//-----------------------------------------------------------------------------
enum class CaseOption : std::size_t
{
    Lower = 0,
    Upper = 1,
    Alt = 2,

    Last = Alt,
    Count = Last + 1
};

//-----------------------------------------------------------------------------
struct WordProperties
{
    static constexpr size_t sTotalCaseOptions_{ 3 };
    static constexpr char sFirstLetter_{ 'a' };
    static constexpr char sLastLetter_{ 'z' };
    static constexpr size_t sTotalLetters_{ static_cast<size_t>(sLastLetter_ - sFirstLetter_ + 1) };
    static constexpr size_t sTotalLengths_{ 50 };
    static constexpr std::string_view sTxtExt_ { ".txt"sv };

    // see windows https://docs.microsoft.com/en-us/windows/win32/fileio/naming-a-file?redirectedfrom=MSDN
    static constexpr std::string_view sIllegalFileNames_[22] = { 
        "CON"sv,
        "PRN"sv,
        "AUX"sv,
        "NUL"sv,
        "COM1"sv,
        "COM2"sv,
        "COM3"sv,
        "COM4"sv,
        "COM5"sv,
        "COM6"sv,
        "COM7"sv,
        "COM8"sv,
        "COM9"sv,
        "LPT1"sv,
        "LPT2"sv,
        "LPT3"sv,
        "LPT4"sv,
        "LPT5"sv,
        "LPT6"sv,
        "LPT7"sv,
        "LPT8"sv,
        "LPT9"sv };

    static constexpr size_t sMaxGeneratedPathName{ 16 };

    // http://pi.math.cornell.edu/~mec/2003-2004/cryptography/subs/frequencies.html
    static constexpr std::array<std::size_t, sTotalLetters_> sLetterUsageFrequency_{
        14810,      // a
        17525,      // b
        22468,      // c
        30342,      // d
        52254,      // e
        56454,      // f
        60147,      // g
        70942,      // h
        84260,      // i
        84448,      // j
        85705,      // k
        92958,      // l
        97719,      // m
        110385,     // n
        124388,     // o
        127704,     // p
        127909,     // q
        138886,     // r
        150336,     // s
        166923,     // t
        172169,     // u
        174188,     // v
        178007,     // w
        178322,     // x
        182175,     // y
        182303      // z
    };

    // https://core.ac.uk/download/pdf/82753461.pdf
    static constexpr std::array<std::size_t, sTotalLengths_> sLengthUsageFrequency_{
        14257,		// 1
        33243,		// 2
        37872,		// 3
        39025,		// 4
        39569,		// 5
        39895,		// 6
        40173,		// 7
        40407,		// 8
        40623,		// 9
        40844,		// 10
        41008,		// 11
        41163,		// 12
        41341,		// 13
        41474,		// 14
        41607,		// 15
        41747,		// 16
        41867,		// 17
        41977,		// 18
        42077,		// 19
        42167,		// 20
        42247,		// 21
        42322,		// 22
        42392,		// 23
        42457,		// 24
        42520,		// 25
        42580,		// 26
        42637,		// 27
        42692,		// 28
        42744,		// 29
        42794,		// 30
        42841,		// 31
        42883,		// 32
        42923,		// 33
        42961,		// 34
        42996,		// 35
        43029,		// 36
        43049,		// 37
        43064,		// 38
        43077,		// 39
        43089,		// 40
        43098,		// 41
        43106,		// 42
        43113,		// 43
        43119,		// 44
        43124,		// 45
        43128,		// 46
        43131,		// 47
        43133,		// 48
        43134,		// 49
        43135,		// 50
    };

    // https://link.springer.com/content/pdf/10.3758/BF03195586.pdf
    // 5263779 + 866156 + 1960412 + 2369820 + 7741842 + 1296925 + 1206747 + 1206747 + 2955858 + 4527332 + 65856 + 460788 + 2553152 + 1467376 + 4535545 + 4729266 + 1255579 + 54221 + 4137949 + 4186210 + 5507692 + 1613323 + 653370 + 1015656 + 123577 + 1062040 + 66423
    // 280937 + 169474 + 229363 + 129632 + 138443 + 100751 + 93212 + 123632 + 223312 + 78706 + 46580 + 106984 + 259474 + 205409 + 105700 + 144239 + 11659 + 146448 + 304971 + 325462 + 57488 + 31053 + 107195 + 7578 + 94297 + 5610
    static constexpr std::array<std::size_t, sTotalCaseOptions_> sCaseOptionsFrequency_{
        62883641,   // 62883641
        66411250,   // + 3527609
        68175054    // + 3527609 / 2
    };

    static constexpr std::array<std::size_t, 4> wordSepsFrequency_ {
        100,
        120,
        130,
        131
    };
    static constexpr std::array<std::string_view, 4> wordSeps_ {
        " "sv,
        ", "sv,
        ","sv,
        ""sv
    };

    static constexpr std::array<std::size_t, 16> eolSepsFrequency_ {
        20,
        25,
        30,
        35,
        40,
        45,
        150,
        230,
        290,
        310,
        340,
        350,
        360,
        270,
        275,
        280,
    };
    static constexpr std::array<std::string_view, 16> eolSeps_ {
        "\n"sv,
        ".\n"sv,
        "!\n"sv,
        "?\n"sv,
        ";\n"sv,
        ":\n"sv,
        ". "sv,
        "! "sv,
        "? "sv,
        "; "sv,
        ": "sv,
        "."sv,
        "!"sv,
        "?"sv,
        ";"sv,
        ":"sv,
    };

    static constexpr std::size_t eolProbability_{ 20 };

    // dynamic properties
    size_t maxLength_{};
    std::optional<std::size_t> remainingQuotaInBytes_{};

    WordProperties() = delete;
    WordProperties(const WordProperties&) = delete;
    WordProperties(WordProperties&&) = delete;
    WordProperties& operator=(const WordProperties&) = delete;
    WordProperties& operator=(WordProperties&&) = delete;

    WordProperties(const Options& options) :
        remainingQuotaInBytes_(options.quotaInBytes_) {
    }
};

//-----------------------------------------------------------------------------
template <typename TGenerator>
inline std::uint64_t choose(
    TGenerator& rng,
    uint64_t min,
    uint64_t max) noexcept {

    auto chosen = std::uniform_int_distribution<std::uint64_t>(static_cast<std::uint64_t>(0), std::numeric_limits<uint64_t>::max())(rng);

    if (max != std::numeric_limits<uint64_t>::max())
        ++max;
    else if (min > 0)
        --min;

    auto diff = std::max(static_cast<std::uint64_t>(1), max - min);
    auto result = min + (chosen % diff);
#if 0
    std::cout << __LINE__ << " RESULT: " << result << "\n";
#endif //0
    return result;
}

//-----------------------------------------------------------------------------
template <typename TGenerator>
inline bool chooseBernoulli(
    TGenerator& rng,
    uint64_t chanceInMax) noexcept {

    auto result = choose(rng, 0, chanceInMax > 0 ? chanceInMax - 1 : chanceInMax);
    return 0 == result;
}


template<typename IntType = std::size_t, typename RealType = double>
struct binomial_like_distribution
{
    binomial_like_distribution(
        IntType min = 0,
        IntType max = std::numeric_limits<IntType>::max()) noexcept :
        min_(min),
        max_(max),
        diff_(max_ - min_),
        half_(diff_ / static_cast<IntType>(2))
    {
        half_ = std::max(static_cast<IntType>(1), half_);
        halfReal_ = static_cast<RealType>(half_);
    }

    template <typename TGenerator>
    IntType operator()(TGenerator& rng) noexcept {

        RealType invValue = static_cast<RealType>(1.0) / static_cast<RealType>(std::max(static_cast<IntType>(1), static_cast<IntType>(choose(rng, 1, half_)) / static_cast<IntType>(2)));

        if (chooseBernoulli(rng, 2))
            invValue *= static_cast<RealType>(-1.0);

        auto value = invValue * halfReal_;
        value += halfReal_;

        IntType intValue = static_cast<IntType>(value);
        intValue += min_;
        intValue = std::min(intValue, max_);

        return intValue;
    }

private:
    IntType min_{};
    IntType max_{};

    IntType diff_{};
    IntType half_{};

    RealType halfReal_{};
};

//-----------------------------------------------------------------------------
template <typename TGenerator, typename TArrayType>
inline std::size_t pickChoiceFromFrequencyArray(
    TGenerator& rng,
    const TArrayType& array) noexcept {

    auto valueChoice = choose(rng, static_cast<std::uint64_t>(0), static_cast<std::uint64_t>(array[array.size() - 1]));

    auto foundChoice = std::lower_bound(begin(array), end(array), static_cast<std::size_t>(valueChoice));

    if (foundChoice == end(array))
        return array.size() - 1;

    return std::distance(begin(array), foundChoice);
}

//-----------------------------------------------------------------------------
inline void throwStdException(const std::stringstream& ss) noexcept(false) {
    throw std::runtime_error(ss.str());
}

//-----------------------------------------------------------------------------
template <typename TVectorType>
inline void reserveSpace(
    TVectorType& vector,
    std::size_t length,
    const std::string_view errorStr) noexcept(false)
{
    try {
        vector.reserve(length);
    }
    catch (const std::length_error&) {
        throwStdException(SS{} << errorStr << length);
    }
    catch (const std::bad_alloc&) {
        throwStdException(SS{} << errorStr << length);
    }
}

//-----------------------------------------------------------------------------
inline BlobUniPtr readFile(std::filesystem::path filePath) noexcept(false)
{
    static_assert(sizeof(std::byte) == sizeof(char));

    std::ifstream f(filePath, std::ios::binary | std::ios::ate);

    if (!f)
        throwStdException(SS{} << "unable to open file: " << filePath);

    auto pos = f.tellg();
    std::size_t length{ static_cast<decltype(length)>(pos) };

    auto result = std::make_unique<Blob>();

    // the +1 guarentees the nul byte at the end
    reserveSpace(*result, length + 1, (SS{} << "unable to reserve space file: " << filePath << ", size: ").str());
    result->assign(length + 1, std::byte{ 0 });

    f.seekg(0, std::ios::beg);
    f.read(reinterpret_cast<char *>(&((*result)[0])), pos);

    if (!f)
        throwStdException(SS{} << "unable to read entire file: " << filePath);

    f.close();
    if (!f)
        throwStdException(SS{} << "file read error: " << filePath);

    return result;
}


//-----------------------------------------------------------------------------
inline void storeWordsFile(
    std::filesystem::path filePath,
    const StringViewVector &words) noexcept(false)
{
    constexpr std::string_view eol{ "\n"sv };

    static_assert(sizeof(std::byte) == sizeof(char));

    std::size_t count{};

    for (auto& word : words) {
        count += word.length();
        count += eol.length();
    }

    auto blob = std::make_unique<Blob>();

    reserveSpace(*blob, count, "unable to reserve space for sorted words file buffer: "sv);
    blob->assign(count, std::byte{ 0 });

    char* pos = reinterpret_cast<char*>(&((*blob)[0]));

    for (auto& word : words) {
        memcpy(pos, word.data(), sizeof(char) * word.length());
        pos += word.length();

        memcpy(pos, eol.data(), sizeof(char) * eol.length());
        pos += eol.length();
    }

    std::ofstream f(filePath, std::ios::binary | std::ios::out | std::ios::trunc);

    if (!f)
        throwStdException(SS{} << "unable to create or replace file: " << filePath);

    f.write(reinterpret_cast<char*>(&((*blob)[0])), count);

    if (!f)
        throwStdException(SS{} << "unable to write entire file: " << filePath);

    f.close();
    if (!f)
        throwStdException(SS{} << "file write error: " << filePath);
}

//-----------------------------------------------------------------------------
inline std::size_t processWordFileCount(const Blob& blob) noexcept(false)
{
    size_t count{};

    if (blob.size() < 1)
        return count;

    const char* pos = reinterpret_cast<const char*>(&(blob[0]));
    bool foundValue{};

    auto processCount = [&]() noexcept {
        if (foundValue)
            ++count;
        foundValue = {};
    };
    while ('\0' != *pos) {
        if (isspace(*pos)) {
            processCount();
            ++pos;
            continue;
        }
        foundValue = true;
        ++pos;
    }
    processCount();
    return count;
}

//-----------------------------------------------------------------------------
inline std::pair<StringViewVectorUniPtr, std::size_t> processWordFileParse(const Blob& blob) noexcept
{
    auto count = processWordFileCount(blob);

    auto result = std::make_unique<StringViewVector>();
    std::size_t maxLength{};

    reserveSpace(*result, count, "unable to reserve words database of size: "sv);

    const char* pos = reinterpret_cast<const char*>(&(blob[0]));
    const char* start{};

    auto processWord = [&]() noexcept {
        if (!start)
            return;

        decltype(maxLength) distance = static_cast<decltype(maxLength)>(std::distance(start, pos));
        if (distance > maxLength)
            maxLength = distance;

        std::string_view value{ start, static_cast<std::string_view::size_type>(distance) };
        result->push_back(value);

        start = {};
    };

    while ('\0' != *pos) {
        if (isspace(*pos)) {
            processWord();
            ++pos;
            continue;
        }
        if (!start)
            start = pos;
        ++pos;
        continue;
    }
    processWord();
    return { std::move(result), maxLength };
}

//-----------------------------------------------------------------------------
inline std::size_t categorizeCase(char letter) noexcept {
    if (islower(letter)) return 0;
    if (isupper(letter)) return 1;
    return 2;
}

//-----------------------------------------------------------------------------
inline std::size_t categorizeCase(const std::string_view str) noexcept {
    if (str.length() < 1) return 2;
    return categorizeCase(str[0]);
}

//-----------------------------------------------------------------------------
inline void processWordFileSort(StringViewVector& words) noexcept
{
    using CompareType = std::remove_reference_t<decltype(words)>::value_type;
    auto sorter = [&](CompareType& v1, CompareType& v2) noexcept -> bool {
        auto case1 = categorizeCase(v1);
        auto case2 = categorizeCase(v2);

        if (case1 < case2) return true;
        if (case2 < case1) return false;

        if (v1.length() < v2.length()) return true;
        if (v2.length() < v1.length()) return false;

        return v1 < v2;
    };

    std::sort(words.begin(), words.end(), sorter);
}

//-----------------------------------------------------------------------------
inline StringViewVector::const_iterator processWordSearch(
    const StringViewVector& words,
    std::size_t caseOption,
    size_t length,
    char alpha) noexcept
{
    std::string_view empty;

    char buffer[1]{ alpha };

    switch (static_cast<CaseOption>(caseOption)) {
        case CaseOption::Lower:         break;
        case CaseOption::Upper:     buffer[0] = std::toupper(alpha); break;
        case CaseOption::Alt:       {
            if (alpha != WordProperties::sFirstLetter_)
                return words.end();
            buffer[0] = ' ';
            break;
        }
        default: break;
    }

    std::string_view letter(buffer, 1);

    auto searcher = [&](const std::string_view& inV1, const std::string_view& inV2) noexcept -> bool {

        std::string_view v1{ inV1.length() > 0 ? inV1 : letter };
        std::string_view v2{ inV2.length() > 0 ? inV2 : letter };

        size_t length1{ inV1.length() > 0 ? inV1.length() : length };
        size_t length2{ inV2.length() > 0 ? inV2.length() : length };

        auto case1 = categorizeCase(v1);
        auto case2 = categorizeCase(v2);

        if (case1 < case2) return true;
        if (case2 < case1) return false;

        if (length1 < length2) return true;
        if (length2 < length1) return false;

        return v1 < v2;
    };

    auto result = std::lower_bound(begin(words), end(words), empty, searcher);

    if (result == words.end())
        return result;

    if (CaseOption::Alt != static_cast<CaseOption>(caseOption)) {
        if ((*result)[0] != letter[0])
            return words.end();
    }

    if ((*result).length() != length)
        return words.end();

    return result;
}


//-----------------------------------------------------------------------------
struct ChosenWord
{
    std::size_t caseOption_{};
    char letter_{};
    std::size_t length_{};
};

//-----------------------------------------------------------------------------
template <typename TGenerator>
inline ChosenWord chooseWordCombinationBasedOnUsageFrequencies(
    TGenerator& rng,
    const WordProperties &wordProps) noexcept
{
    ChosenWord result;

    result.caseOption_ = pickChoiceFromFrequencyArray(rng, WordProperties::sCaseOptionsFrequency_);
    result.letter_ = static_cast<decltype(WordProperties::sFirstLetter_)>(WordProperties::sFirstLetter_ + static_cast<decltype(WordProperties::sFirstLetter_ )>(pickChoiceFromFrequencyArray(rng, WordProperties::sLetterUsageFrequency_)));
    result.length_ = std::min(1 + pickChoiceFromFrequencyArray(rng, WordProperties::sLengthUsageFrequency_), wordProps.maxLength_);

    if (result.caseOption_ > 1)
        result.letter_ = WordProperties::sFirstLetter_;

    return result;
}


//-----------------------------------------------------------------------------
template <typename TGenerator>
inline std::size_t chooseWordBasedOnUsageFrequencies(
    TGenerator& rng,
    const StringViewVector& words,
    const WordProperties& wordProps,
    std::vector<StringViewVector::const_iterator>& allCombinationsStart,
    std::vector<StringViewVector::const_iterator>& allCombinationsEnd) noexcept {

    constexpr auto combinations = WordProperties::sTotalCaseOptions_ * WordProperties::sTotalLengths_ * WordProperties::sTotalLetters_;

    while (true) {
        auto chosen = chooseWordCombinationBasedOnUsageFrequencies(rng, wordProps);

        auto chosenCombo = (chosen.caseOption_ * (WordProperties::sTotalLengths_ * WordProperties::sTotalLetters_)) +
            (static_cast<std::size_t>(chosen.length_)* (WordProperties::sTotalLetters_)) +
            (static_cast<std::size_t>(chosen.letter_ - WordProperties::sFirstLetter_));

        assert(chosenCombo < combinations);
        auto foundStart = allCombinationsStart[chosenCombo];
        auto foundEnd = allCombinationsEnd[chosenCombo];

        if (foundStart == words.end())
            continue;

        auto available = std::distance(foundStart, foundEnd);
        assert(available > 0);

        auto chosenWordIndex = choose(rng, static_cast<std::uint64_t>(0), static_cast<std::uint64_t>(available - 1));
        auto chosenWordIter = foundStart + static_cast<std::size_t>(chosenWordIndex);

        return std::distance(begin(words), chosenWordIter);
    }
    return 0;
}


//-----------------------------------------------------------------------------
template <typename TGenerator>
inline std::size_t chooseWordCombinationBasedOnEqualDistribution(
    TGenerator& rng,
    const StringViewVector& words,
    const WordProperties& wordProps,
    std::size_t &ioAllowedCaseOptions) noexcept
{
    constexpr std::size_t tooManyAttemptsToFindCaseOption = 300;

    std::size_t attemptsPerCase[static_cast<std::underlying_type_t<CaseOption>>(CaseOption::Count)]{};

    while (true) {

        auto useCaseOption = pickChoiceFromFrequencyArray(rng, WordProperties::sCaseOptionsFrequency_);

        std::size_t useBit = (static_cast<std::size_t>(1) << useCaseOption);

        // no options for this case may be available
        if (0 == (ioAllowedCaseOptions & useBit))
            continue;

        while (true) {

            auto chosen = choose(rng, static_cast<std::uint64_t>(0), static_cast<std::uint64_t>(words.size() - 1));
            auto word = words[static_cast<std::size_t>(chosen)];
            auto caseOption = categorizeCase(word);

            if (caseOption != useCaseOption) {
                ++attemptsPerCase[caseOption];
                if (attemptsPerCase[caseOption] > tooManyAttemptsToFindCaseOption) {
                    ioAllowedCaseOptions = ioAllowedCaseOptions ^ useBit;
                    break;
                }
                continue;
            }
            return chosen;
        }
    }
    return 0;
}

//-----------------------------------------------------------------------------
template <typename TGenerator>
inline StringViewVectorUniPtr generateZipfWords(
    TGenerator& rng,
    const StringViewVector& words,
    const WordProperties& wordProps,
    std::vector<StringViewVector::const_iterator>& allCombinationsStart,
    std::vector<StringViewVector::const_iterator>& allCombinationsEnd) noexcept(false)
{
    constexpr std::size_t tooManyConflictsInARow = 100;

    auto result = std::make_unique<StringViewVector>();

    auto expectingCount = words.size() / 2;

    // specialized dynamic bitset
    std::vector<bool> filled;

    reserveSpace(filled, words.size(), "unable to reserve zipf words of size: "sv);
    reserveSpace(*result, expectingCount, "unable to reserve zipf words of count: "sv);
    filled.assign(words.size(), false);

    std::size_t allowedCaseOptions{std::numeric_limits<std::size_t>::max()};
    static_assert(sizeof(allowedCaseOptions) * 8 > static_cast<std::underlying_type_t<CaseOption>>(CaseOption::Last));

    size_t conflictsInARow{};
    while (result->size() < expectingCount) {
        std::size_t chosenWord{};

        if (conflictsInARow < tooManyConflictsInARow) {
            chosenWord = chooseWordBasedOnUsageFrequencies(rng, words, wordProps, allCombinationsStart, allCombinationsEnd);
        } else {
            chosenWord = chooseWordCombinationBasedOnEqualDistribution(rng, words, wordProps, allowedCaseOptions);
        }

        assert(chosenWord < words.size());

        if (filled[chosenWord]) {
            ++conflictsInARow;
            continue;
        }

        result->push_back(words[chosenWord]);
        filled[chosenWord] = true;
        conflictsInARow = conflictsInARow < tooManyConflictsInARow ? 0 : conflictsInARow;
    }

    return result;
}

//-----------------------------------------------------------------------------
inline void processWordSearch(
    const StringViewVector& words,
    const WordProperties& wordProps,
    std::vector<StringViewVector::const_iterator>& outAllCombinationsStart,
    std::vector<StringViewVector::const_iterator>& outAllCombinationsEnd
) noexcept
{
    constexpr auto combinations = WordProperties::sTotalCaseOptions_ * WordProperties::sTotalLengths_ * WordProperties::sTotalLetters_;

    outAllCombinationsStart.reserve(combinations);
    outAllCombinationsEnd.reserve(combinations);

    for (size_t caseOption{}; caseOption < WordProperties::sTotalCaseOptions_; ++caseOption) {
        for (size_t length = 1; length <= WordProperties::sTotalLengths_; ++length) {
            for (std::remove_const_t<decltype(WordProperties::sFirstLetter_)> letter{ WordProperties::sFirstLetter_ }; letter <= WordProperties::sLastLetter_; ++letter) {
                auto result = processWordSearch(words, caseOption, length, letter);
                outAllCombinationsStart.push_back(result);
            }
        }
    }

    for (auto iter{ outAllCombinationsStart.begin() }; iter != outAllCombinationsStart.end(); ++iter) {
        auto& iterWords = *iter;

        // if no starting point then an ending point can't exist either
        if (iterWords == words.end()) {
            outAllCombinationsEnd.push_back(words.end());
            continue;
        }

        auto aheadIter = iter;
        ++aheadIter;
        if (aheadIter == outAllCombinationsStart.end()) {
            outAllCombinationsEnd.push_back(words.end());
            continue;
        }

        bool found{};
        for (; aheadIter != outAllCombinationsStart.end(); ++aheadIter) {
            auto& aheadIterWords = (*aheadIter);
            if (aheadIterWords == words.end())
                continue;

            outAllCombinationsEnd.push_back(aheadIterWords);
            found = true;
            break;
        }
        if (!found)
            outAllCombinationsEnd.push_back(words.end());
    }
}

//-----------------------------------------------------------------------------
inline void processWordDisplay(const StringViewVector& words) noexcept
{
    for (auto& word : words) {
        std::cout << "WORD: " << word << "\n";
    }
}

//-----------------------------------------------------------------------------
inline void processWordDisplaySplitPoints(
    const StringViewVector& words,
    const std::vector<StringViewVector::const_iterator>& wordSplitPointsStart,
    const std::vector<StringViewVector::const_iterator>& wordSplitPointsEnd
) noexcept
{
    assert(wordSplitPointsStart.size() == wordSplitPointsEnd.size());

    auto iter1 = wordSplitPointsStart.begin();
    auto iter2 = wordSplitPointsEnd.begin();

    for (; iter1 != wordSplitPointsStart.end() && iter2 != wordSplitPointsEnd.end(); ++iter1, ++iter2) {
        std::string value1 = (*iter1) == words.end() ? std::string{} : std::string{ **iter1 };
        std::string value2 = (*iter2) == words.end() ? std::string{} : std::string{ **iter2 };

        if (!value1.empty())
            std::cout << "SPLIT: [" << value1 << " , " << value2 << ")\n";
    }
}

//-----------------------------------------------------------------------------
inline std::pair<BlobUniPtr, StringViewVectorUniPtr> processWordFile(
    const Options& options,
    WordProperties& wordProps) noexcept(false) {
    BlobUniPtr blob = readFile(options.wordsFilePath_);
    auto [words, maxLength] = processWordFileParse(*blob);
    if (!options.wordsSorted_)
        processWordFileSort(*words);

    wordProps.maxLength_ = maxLength;

    assert(WordProperties::sTotalLengths_ > wordProps.maxLength_);

    return { std::move(blob), std::move(words) };
}

//-----------------------------------------------------------------------------
inline void processWordCombinations(
    const StringViewVector& words,
    const WordProperties& wordProps,
    std::vector<StringViewVector::const_iterator>& outAllCombinationsStart,
    std::vector<StringViewVector::const_iterator>& outAllCombinationsEnd) noexcept {

    processWordSearch(words, wordProps, outAllCombinationsStart, outAllCombinationsEnd);
    //processWordDisplaySplitPoints(words, outAllCombinationsStart, outAllCombinationsEnd);
}

//-----------------------------------------------------------------------------
template <typename TGenerator>
inline void writeRandomizedWordsFile(
    TGenerator& rng,
    const Options& options,
    const StringViewVector& words,
    WordProperties& wordProps,
    const std::filesystem::path& filePath,
    const std::size_t inCount) {

    if (wordProps.remainingQuotaInBytes_.has_value()) {
        // stop generating files if there's no more quote room left
        if (wordProps.remainingQuotaInBytes_.value() < 0)
            return;
    }

    constexpr std::size_t maxWordsPerWrite = 0xFFFF;
    auto remaining{ inCount };

    TGenerator finalRngState(rng);

    auto eolDist = [](TGenerator &rng) { return choose(rng, static_cast<std::uint64_t>(0), static_cast<std::uint64_t>(WordProperties::eolProbability_)); };

    std::optional<std::ifstream> fRead;
    std::optional<std::ofstream> fWrite;
    if (options.validateOnly_) {
        fRead.emplace(filePath, std::ios::binary | std::ios::in);
        if (!(*fRead))
            throwStdException(SS{} << "validation failed - unable to read file: " << filePath);
    }
    else {
        fWrite.emplace(filePath, std::ios::binary | std::ios::out | std::ios::trunc);
        if (!(*fWrite))
            throwStdException(SS{} << "unable to create or replace file: " << filePath);
    }

    while (remaining > 0) {
        TGenerator activeRngState(finalRngState);

        std::size_t count{};
        auto blob = std::make_shared<Blob>();
        char* pos{};

        std::function<void()> preFunc;
        std::function<void()> postFunc;
        std::function<void(const std::string_view&)> applyFunc;

        auto countPreLamda = [&]() noexcept {
        };
        auto countPostLamda = [&]() noexcept {
        };

        auto storePreLamda = [&]() noexcept(false) {
            reserveSpace(*blob, count, "unable to generate file of size: "sv);
            blob->assign(count, std::byte{ 0 });
            pos = reinterpret_cast<char*>(&((*blob)[0]));
        };
        auto storePostLamda = [&]() noexcept(false) {

            if (options.validateOnly_) {
                Blob readBuf;
                reserveSpace(readBuf, blob->size(), "validation failed due to inability to reserve read space of: "sv);
                readBuf.assign(blob->size(), std::byte{ 0 });
                fRead->read(reinterpret_cast<char*>(&(readBuf[0])), count);
                if (!(*fRead))
                    throwStdException(SS{} << "validation failed as read file failed: "sv << filePath);

                if (0 != memcmp(&(readBuf[0]), &((*blob)[0]), blob->size()))
                    throwStdException(SS{} << "validation failed as file contents are not as expected: "sv << filePath);
            }
            else {
                fWrite->write(reinterpret_cast<char*>(&((*blob)[0])), count);
                if (!(*fWrite))
                    throwStdException(SS{} << "unable to write entire file: "sv << filePath);
            }
        };

        auto countLambda = [&](const std::string_view& value) noexcept {
            count += value.length();
        };
        auto storeLambda = [&](const std::string_view& value) noexcept {
            memcpy(pos, value.data(), sizeof(char) * value.length());
            pos += value.length();
        };

        auto preQuota{ wordProps.remainingQuotaInBytes_ };
        auto preRemaining{ remaining };

        for (int loop = 0; loop < 2; ++loop) {

            remaining = preRemaining;
            wordProps.remainingQuotaInBytes_ = preQuota;
            zipf_distribution<std::uint64_t> zipfDist(static_cast<std::uint64_t>(words.size() - 1));
            auto uniformDist = [&words](TGenerator& rng) { return choose(rng, static_cast<std::uint64_t>(0), static_cast<std::uint64_t>(words.size() - 1)); };

            TGenerator useRng(activeRngState);

            if (0 == loop) {
                preFunc = countPreLamda;
                postFunc = countPostLamda;
                applyFunc = countLambda;
            }
            else {
                preFunc = storePreLamda;
                postFunc = storePostLamda;
                applyFunc = storeLambda;
            }

            preFunc();

            std::size_t processed{};
            while (remaining > 0) {
                auto selection = options.zipfDistribution_ ? zipfDist(useRng) : uniformDist(useRng);
                auto& word = words[static_cast<std::size_t>(selection)];

                auto length = word.length();

                applyFunc(word);
                --remaining;
                ++processed;

                if (0 == eolDist(useRng)) {
                    auto eolChoice = pickChoiceFromFrequencyArray(useRng, WordProperties::eolSepsFrequency_);
                    auto eolStr = WordProperties::eolSeps_[eolChoice];
                    applyFunc(eolStr);

                    length += eolStr.length();
                }
                else {
                    auto sepChoice = pickChoiceFromFrequencyArray(useRng, WordProperties::wordSepsFrequency_);
                    auto sepStr = WordProperties::wordSeps_[sepChoice];
                    applyFunc(sepStr);

                    length += sepStr.length();
                }

                if (wordProps.remainingQuotaInBytes_.has_value()) {
                    if (length > wordProps.remainingQuotaInBytes_.value()) {
                        remaining = 0;
                        wordProps.remainingQuotaInBytes_ = static_cast<decltype(wordProps.remainingQuotaInBytes_)::value_type>(0);
                        break;
                    }
                    wordProps.remainingQuotaInBytes_.value() -= length;
                }

                if (processed == maxWordsPerWrite)
                    break;
            }

            postFunc();
            finalRngState = useRng;
        }
    }

    if (options.validateOnly_) {
        std::byte eofBuffer[1]{};
        fRead->read(reinterpret_cast<char*>(&(eofBuffer[0])), sizeof(eofBuffer));
        if (!fRead->eof())
            throwStdException(SS{} << "validation failed - file contains more contents than expected: "sv << filePath);
        fRead->clear();
        fRead->close();
        if (!(*fRead))
            throwStdException(SS{} << "validation failed - file read error: "sv << filePath);
    }
    else {
        fWrite->close();
        if (!(*fWrite))
            throwStdException(SS{} << "file write error: "sv << filePath);
    }

    rng = finalRngState;
}

//-----------------------------------------------------------------------------
template <typename TGenerator>
inline void writeRandomizedWordsFile(
    TGenerator& rng,
    const Options& options,
    const StringViewVector& words,
    WordProperties& wordProps,
    const std::filesystem::path& filePath)
{
    auto dist = [](TGenerator & rng) { return chooseBernoulli(rng, 4); };

    std::size_t wordsInFile{};
    if (dist(rng)) {

        binomial_like_distribution<std::uint64_t> dist(1, options.maxWordsPerFile_);
        wordsInFile = dist(rng);
    }
    else {
        wordsInFile = static_cast<std::size_t>(choose(rng, static_cast<std::uint64_t>(0), static_cast<std::uint64_t>(options.maxWordsPerFile_)));
    }

    writeRandomizedWordsFile(rng, options, words, wordProps, filePath, wordsInFile);
}

//-----------------------------------------------------------------------------
template <typename TGenerator>
inline void writeRandomizedWordsPaths(
    TGenerator& rng,
    const Options& options,
    const StringViewVector& words,
    WordProperties& wordProps,
    const std::filesystem::path& filePath,
    std::vector<std::size_t> totalPaths,
    std::vector<std::size_t> totalPathFiles) noexcept(false) {

    decltype(totalPaths)::value_type totalPathsAtCurrentLevel{};
    decltype(totalPathFiles)::value_type totalPathFilesAtCurrentLevel{};

    if (totalPaths.size() > 0) {
        totalPathsAtCurrentLevel = totalPaths[0];
        totalPaths.erase(totalPaths.begin());
    }
    if (totalPathFiles.size() > 0) {
        totalPathFilesAtCurrentLevel = totalPathFiles[0];
        totalPathFiles.erase(totalPathFiles.begin());
    }

    auto lowerUpperDist = [](TGenerator& rng) { return chooseBernoulli(rng, 4); };

    auto letDistLower = [](TGenerator& rng) { return choose(rng, static_cast<std::uint64_t>(WordProperties::sFirstLetter_), static_cast<std::uint64_t>(WordProperties::sLastLetter_)); };
    auto letDistUpper = [](TGenerator& rng) { return choose(rng, static_cast<std::uint64_t>(std::toupper(WordProperties::sFirstLetter_)), static_cast<std::uint64_t>(std::toupper(WordProperties::sLastLetter_))); };
    auto lengthDist = [](TGenerator& rng) { return choose(rng, static_cast<std::uint64_t>(1), static_cast<std::uint64_t>(WordProperties::sMaxGeneratedPathName)); };

    std::set<std::string> files;

    auto isCaseInsensativeUnique = [&](const std::string& inFileStr) noexcept -> bool {
        std::string fileStr{ inFileStr };
        std::transform(begin(fileStr), end(fileStr), begin(fileStr), [](char let) noexcept -> char { return static_cast<char>(std::tolower(let)); });
        if (files.end() != files.find(fileStr))
            return false;

        files.insert(fileStr);
        return true;
    };

    auto randomStrGen = [&](std::size_t length) noexcept -> std::string {
        std::string result;
        result.reserve(length);

        while (length > 0) {
            if (lowerUpperDist(rng))
                result += static_cast<char>(letDistUpper(rng));
            else
                result += static_cast<char>(letDistLower(rng));
            --length;
        }
        return result;
    };

    for (auto& illegalFileName : WordProperties::sIllegalFileNames_) {
        isCaseInsensativeUnique(std::string{ illegalFileName });
    }

    for (decltype(totalPathFilesAtCurrentLevel) loop = 0; loop < totalPathFilesAtCurrentLevel; ++loop) {
        std::string nameStr;
        while (true) {
            nameStr = randomStrGen(static_cast<std::size_t>(lengthDist(rng)));
            if (!isCaseInsensativeUnique(nameStr))
                continue;
            break;
        }

        std::filesystem::path name{ nameStr + std::string{ WordProperties::sTxtExt_ } };
        std::filesystem::path usePath{ filePath };
        usePath /= name;

        writeRandomizedWordsFile(rng, options, words, wordProps, usePath);
    }

    files.clear();
    for (decltype(totalPathsAtCurrentLevel) loop = 0; loop < totalPathsAtCurrentLevel; ++loop) {
        std::string nameStr;
        while (true) {
            nameStr = randomStrGen(static_cast<std::size_t>(lengthDist(rng)));
            if (!isCaseInsensativeUnique(nameStr))
                continue;
            break;
        }

        std::filesystem::path name{ randomStrGen(static_cast<std::size_t>(lengthDist(rng))) };
        std::filesystem::path usePath{ filePath };
        usePath /= name;

        if (!std::filesystem::exists(usePath)) {
            if (options.validateOnly_) {
                throwStdException(SS{} << "validation failed - direction was not generated: " << usePath);
            }
            if (!std::filesystem::create_directory(usePath))
                throwStdException(SS{} << "unable to create directory: " << usePath);
        }
        writeRandomizedWordsPaths(rng, options, words, wordProps, usePath, totalPaths, totalPathFiles);
    }
}

//-----------------------------------------------------------------------------
template <typename TGenerator>
inline void writeRandomizedWordsPaths(
    TGenerator& rng,
    const Options& options,
    const StringViewVector& words,
    WordProperties& wordProps,
    const std::filesystem::path& filePath) noexcept(false) {

    writeRandomizedWordsPaths(rng, options, words, wordProps, filePath, options.totalPaths_, options.totalPathFiles_);
}

//-----------------------------------------------------------------------------
template <typename TGenerator>
inline void generate(const Options& options) noexcept(false)
{
    TGenerator generator(options.seed_);

    WordProperties wordProps(options);
    StringViewVectorUniPtr words;
    StringViewVectorUniPtr chosenWords;

    if (!options.wordsFilePath_.empty()) {
        auto [blob, words] = processWordFile(options, wordProps);

        if (!options.wordsZipf_) {
            if (!options.saveSortedWordsFilePath_.empty())
                storeWordsFile(options.saveSortedWordsFilePath_, *words);
        }

        if (options.zipfDistribution_) {
            if (!options.wordsZipf_) {
                std::vector<StringViewVector::const_iterator> wordCombosStart;
                std::vector<StringViewVector::const_iterator> wordCombosEnd;

                processWordCombinations(*words, wordProps, wordCombosStart, wordCombosEnd);
                chosenWords = generateZipfWords(generator, *words, wordProps, wordCombosStart, wordCombosEnd);
            }
            else {
                // clone the existing words as the zipf words
                chosenWords = std::make_unique<StringViewVector>(*words);
            }
            if (!options.saveZipfWordsFilePath_.empty())
                storeWordsFile(options.saveZipfWordsFilePath_, *chosenWords);
        }

        for (auto& file : options.outputFileFilePaths_) {
            writeRandomizedWordsFile(generator, options, options.zipfDistribution_ ? *chosenWords : *words, wordProps, file);
        }

        for (auto& path : options.outputPaths_) {
            writeRandomizedWordsPaths(generator, options, options.zipfDistribution_ ? *chosenWords : *words, wordProps, path);
        }
    }
}

} // namespace zipf
